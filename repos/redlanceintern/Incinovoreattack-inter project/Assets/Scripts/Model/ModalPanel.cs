﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using System;

public class ModalPanel : MonoBehaviour {

    public Text question;
    //public Image iconImage;
    public Button confirmButton;
    public Button yesButton;
    public Button noButton;
    public Button cancelButton;
    public GameObject modalPanelObject;

    private static ModalPanel modalPanel;

    public static ModalPanel Instance()
    {
        if (!modalPanel)
        {
            modalPanel = FindObjectOfType(typeof(ModalPanel)) as ModalPanel;
            if (!modalPanel)
                Debug.LogError("There needs to be one active ModalPanel script on a GameObject in your scene.");
        }

        return modalPanel;
    }

    // Yes/No/Cancel: A string, a Yes event, a No event and Cancel event
    public void Choice(string question, UnityAction yesEvent, UnityAction noEvent, UnityAction cancelEvent, UnityAction confirmEvent)
    {
        modalPanelObject.SetActive(true);

        yesButton.onClick.RemoveAllListeners();
        yesButton.onClick.AddListener(yesEvent);
        yesButton.onClick.AddListener(ClosePanel);

        noButton.onClick.RemoveAllListeners();
        noButton.onClick.AddListener(noEvent);
        noButton.onClick.AddListener(ClosePanel);

        cancelButton.onClick.RemoveAllListeners();
        cancelButton.onClick.AddListener(cancelEvent);
        cancelButton.onClick.AddListener(ClosePanel);

        confirmButton.onClick.RemoveAllListeners();
        confirmButton.onClick.AddListener(confirmEvent);
        confirmButton.onClick.AddListener(ClosePanel);

        this.question.text = question;
		//yesButton.GetComponent(Text).guiText= "Buy Lances";
		//yesButton.GetComponentInChildren(Text).text = "BuyLances";
		//yestButton.trasphorm.GetComponent.GetChildrent.txt
		yesButton.transform.GetChild(0).gameObject.GetComponent<Text>().text="Buy Lances";

		//this.iconImage.gameObject.SetActive(false);
		if (yesEvent != null) 
			yesButton.gameObject.SetActive (true);
		if (noEvent != null) 
			noButton.gameObject.SetActive(true);
		if (cancelEvent != null) 
        	cancelButton.gameObject.SetActive(true);
		if (confirmEvent != null) 
       		confirmButton.gameObject.SetActive(true);
    }

    internal void Choice(string v1, Action testYesFunction, Action testNoFunction, Action testCancelFunction, object v2)
    {
        throw new NotImplementedException();
    }

    public void ClosePanel()//close pannel
    {
        modalPanelObject.SetActive(false);
		confirmButton.gameObject.SetActive (false);
		yesButton.gameObject.SetActive (false);
		noButton.gameObject.SetActive (false);
		cancelButton.gameObject.SetActive (false);
		//set all children to false
    }
}