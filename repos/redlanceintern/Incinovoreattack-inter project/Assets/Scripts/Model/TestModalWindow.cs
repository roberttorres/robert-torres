﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class TestModalWindow : MonoBehaviour {

    private ModalPanel modalPanel;
    private DisplayManager displayManager;
    private UnityAction myYesAction;
    private UnityAction myNoAction;
    private UnityAction myCancelAction;
    private UnityAction myConfirmAction;
	int Lances;

    void Awake()
    {
        modalPanel = ModalPanel.Instance();
        displayManager = DisplayManager.Instance();
        myYesAction = new UnityAction(TestYesFunction);
        myNoAction = new UnityAction(TestNoFunction);
        myCancelAction = new UnityAction(TestCancelFunction);
        myConfirmAction = new UnityAction(TestConfirmFunction);
    }

    //  Send to the Modal Panel to set up the Buttons and Functions to call
    public void ActivatePanel()
    {
		Awake ();
		modalPanel.Choice("Test Out Pannel", null, null, TestCancelFunction, TestConfirmFunction);
    }
	public void ActivatePanelBuyLances()
	{
		Awake ();
		modalPanel.Choice("Would you like to buy Lances?",TestYesFunction, null, TestCancelFunction,TestConfirmFunction);
	}



    //  These are wrapped into UnityActions
    void TestYesFunction()
    {
        displayManager.DisplayMessage("Yes was pressed");
		Lances = Lances + 1;

    }

    void TestNoFunction()
    {
        displayManager.DisplayMessage("No");
    }

    public void TestCancelFunction()
    {
        displayManager.DisplayMessage("Cancel");
        modalPanel.ClosePanel();
    }
    void TestConfirmFunction()
    {
        displayManager.DisplayMessage("Confirm was pressed");
    }

	~TestModalWindow()
	{
		myYesAction = null;
		myNoAction = null;
		myCancelAction = null;
		myConfirmAction = null;
	}
}