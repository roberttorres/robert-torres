﻿using UnityEngine;
using System.Collections;

public class Modal : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnGUI()
    {
        ShowModal();
    }

    public void ShowModal()
    {
        GUI.BeginGroup(new Rect((Screen.width - 400) / 2, (Screen.height - 200) / 2, 400, 200));
        GUI.Box(new Rect(0, 0, 400, 200), "");
        GUI.Label(new Rect((400-230)/2,(200-30)/2,230,30),"Are you sure you want to do this?");
        GUI.Button(new Rect(20, 85 + 40, 150, 30), "Yes");
        GUI.Button(new Rect((400 - 230) / 2 + 150, (200 - 30) / 2 + 40, 150, 30), "No");
        GUI.EndGroup();
    }
}
