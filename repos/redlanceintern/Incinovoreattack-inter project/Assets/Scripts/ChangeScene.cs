﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    public void ChangeToSceneShop()
    {
       SceneManager.LoadScene("TheShop");
    }

    public void ChangeToSceneBadges()
    {
        SceneManager.LoadScene("TheBadges");
    }

    public void ChangeToSceneMain()
    {
       SceneManager.LoadScene("Main");
    }

    public void ChangeToSceneBoost()
    {
        SceneManager.LoadScene("Boost");
    }

    public void ChangeToSceneLifeForce()
    {
        SceneManager.LoadScene("LifeForce");
    }

    public void ChangeToSceneCurrency()
    {
        SceneManager.LoadScene("Currency");
    }

    public void ChangeToSceneHeroes()
    {
        SceneManager.LoadScene("Hero");
    }
}





