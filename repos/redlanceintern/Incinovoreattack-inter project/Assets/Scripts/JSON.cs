﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class JsonClass : MonoBehaviour {



	JsonClass j;
	int PlayCoins;
	int PlayLances;
	string PlayEmail;

	struct CharacterData
	{
		string PlayDefault;//OBJ
		bool Unlocked;//hero info
		int Kills;//hero info
		int LifeForce;//hero info
		bool LockAmplifier;
		bool EquipAmplifier;
		int LvlAmplifier;
		bool LockShield;
		bool EquipShield;
		int LvlShield;
		bool LockRecovery;
		bool EquipRecovery;
		int LvlRecovery;
		bool LockBolden;
		bool EquipBolden;
		int LvlBolden;
	}

	CharacterData []Charater= new CharacterData[3];


	bool Badge1;
	bool Badge2;
	bool Badge3;
	bool Badge4;
	bool Badge5;

	JsonClass()
	{
		 j= new JsonClass();
	}

	string ReturnJsonString()
	{
		string json = JsonUtility.ToJson (j);
		return json;
	}
	JsonClass ReturnJsonObj(string json)
	{
		j = JsonUtility.FromJson<JsonClass>(json);
		return j; 
	}


}