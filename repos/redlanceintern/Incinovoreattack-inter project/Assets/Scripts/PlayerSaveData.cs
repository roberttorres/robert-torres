﻿using UnityEngine;
using System.Collections;

public class PlayerSaveData : MonoBehaviour {




		public HeroData cinderHawk, stoneFish, sonaru;
		public HeroData[] heroes= new HeroData[3];
		public PlayerSaveData PSD = new PlayerSaveData();
		public HeroData HD;
		//heroes = new HeroData[3];




		public int PlayCoins;
		public int PlayLances;
		public string PlayEmail;
		public static bool Badge1;
		public static bool Badge2;
		public static bool Badge3;
		public static bool Badge4;
		public static bool Badge5;

		public struct HeroData
		{
			string PlayDefault;//OBJ
			bool Unlocked;//hero info
			int Kills;//hero info
			int LifeForce;//hero info
			bool LockAmplifier;
			bool EquipAmplifier;
			int LvlAmplifier;
			bool LockShield;
			bool EquipShield;
			int LvlShield;
			bool LockRecovery;
			bool EquipRecovery;
			int LvlRecovery;
			bool LockBolden;
			bool EquipBolden;
			int LvlBolden;
		}

		//****************************************************************

		public PlayerSaveData()
		{
			//PSD = new PlayerSaveData();
			//HD = new HeroData[1];
		PlayLances = 5;
		PlayEmail = "Robyt30@gmail";
		}
	
		public void LoadPlayerData()
		{
			PlayCoins = PlayerPrefs.GetInt ("PlayCoins");
			PlayerPrefs.GetInt ("PlayLances",PlayLances);
			PlayerPrefs.GetString ("PlayEmail",PlayEmail);
		}
		public void SavePlayerData()
		{
			PlayerPrefs.SetInt("PlayCoins",PlayCoins);
			PlayerPrefs.SetInt ("PlayLances",PlayLances);
			PlayerPrefs.SetString ("PlayEmail",PlayEmail);
		}

	public void PrintOBJ()
	{
		PSD = createSaveDataFromJSON ();
		Debug.Log(PSD.PlayCoins);
	}


		//********************************************************************************************************


		public static PlayerSaveData createSaveDataFromJSON()
		{
			return JsonUtility.FromJson<PlayerSaveData> (PlayerPrefs.GetString ("playerSaveData"));
			//returns the JSON back into a OBJ
		}

		public static void saveToJson(PlayerSaveData psd)
		{
			string toJson = JsonUtility.ToJson (psd);// moves obj to JSON format
			Debug.Log (toJson);//debug statement
			PlayerPrefs.SetString ("playerSaveData", toJson);//saves player data 
		}

		public void updateData()
		{
			//playerCoins += PlayerStatus.coins;
			//heroes [playerDefault].kills += PlayerStatus.kills;
			saveToJson (this);
		}

		public void AddCoin()
		{
			PlayCoins++;
		}
		public void HeroKillsAdd()
		{
			
		}




}
