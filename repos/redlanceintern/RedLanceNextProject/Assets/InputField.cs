﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Globalization;


public class InputField : MonoBehaviour {


	public int FinalAttackValue,FinalStrengthValue,FinalDodgeValue,FinalDefendValue;



	public void RollAttack() {
		Text AttackValueText= GameObject.Find("AttackValueText").GetComponent<Text>();//takes the number inserted into the inputField 
		string AV = AttackValueText.text;//puts the gameobject text into a string 
		string s = AV.ToString();//converts the value into a string
		string[] parts = s.Split ('.');//built in function to split on a given character('.')
		int i1 = int.Parse (parts[0]);//left of decimal
		int i2 = int.Parse (parts[1]);//right of decimal


		int max =Random.Range(i1,6);//find max rolls
		int rawRoll = 0; // intitalize temp roll holder 
		for (int a= 0; a<=max; a++)//loop to get each rool value
		{
			rawRoll =Random.Range(1,6);//actuall dice roll
			FinalAttackValue = FinalAttackValue + rawRoll;//adding roll to the total
		}
		FinalAttackValue = FinalAttackValue + i2;// adding bonous to the overall value



	}
	public void RollDefend() {
		Text DefendValueText= GameObject.Find("DefendValueText").GetComponent<Text>();   
		string DV = DefendValueText.text;
		string s = DV.ToString();
		string[] parts = s.Split ('.');
		int i1 = int.Parse (parts[0]);
		int i2 = int.Parse (parts[1]);
		FinalDefendValue =Random.Range(i1,7);


		int max =Random.Range(i1,7);//find max rolls
		int rawRoll = 0; // intitalize temp roll holder 
		for (int a= 0; a<=max; a++)//loop to get each rool value
		{
			rawRoll =Random.Range(1,6);//actuall dice roll
			FinalDefendValue = FinalDefendValue + rawRoll;//adding roll to the total
		}
		FinalDefendValue = FinalDefendValue + i2;// adding bonous to the overall value
	}


	public void RollDodge ()
	{
		Text DodgeValueText = GameObject.Find ("DodgeValueText").GetComponent<Text> ();   
		string DGV = DodgeValueText.text;
		string s = DGV.ToString ();
		string[] parts = s.Split ('.');
		int i1 = int.Parse (parts [0]);
		int i2 = int.Parse (parts [1]);
		FinalDodgeValue = Random.Range (i1, 7);

		int max =Random.Range(i1,6);//find max rolls
		int rawRoll = 0; // intitalize temp roll holder 
		for (int a= 0; a<=max; a++)//loop to get each rool value
		{
			rawRoll =Random.Range(1,6);//actuall dice roll
			FinalDodgeValue = FinalDodgeValue + rawRoll;//adding roll to the total
		}
		FinalDodgeValue = FinalDodgeValue + i2;// adding bonous to the overall value
	}

	public void RollStrength ()
	{
		Text StrengthValueText = GameObject.Find ("StrengthValueText").GetComponent<Text> ();   
		string SV = StrengthValueText.text;
		string s = SV.ToString ();
		string[] parts = s.Split ('.');
		int i1 = int.Parse (parts [0]);
		int i2 = int.Parse (parts [1]);
		FinalStrengthValue = Random.Range (i1, i2);

		int max =Random.Range(i1,6);//find max rolls
		int rawRoll = 0; // intitalize temp roll holder 
		for (int a= 0; a<=max; a++)//loop to get each rool value
		{
			rawRoll =Random.Range(1,6);//actuall dice roll
			FinalStrengthValue = FinalStrengthValue + rawRoll;//adding roll to the total
		}
		FinalStrengthValue = FinalStrengthValue + i2;// adding bonous to the overall value
	}




	public void Battle()
	{
		int APower =FinalAttackValue*FinalStrengthValue;
		int DPower = FinalDodgeValue*FinalDefendValue;
	
		if (FinalAttackValue > FinalDodgeValue) {
			int Damage = APower - DPower;
			Debug.Log ("You Damage Delt Is" + Damage);
		
		} else 
		{
			Debug.Log ("Your Attack Missed!");
		}
	}


}